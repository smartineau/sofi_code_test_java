package tests;

/*
 *
 */

import io.restassured.response.Response;
import static io.restassured.RestAssured.*;

public class TestBase {

    public static String api_key = "cfdf32e45e78c4bb06c473806ba5298d";

    //tvdb uses a JWT with a 24 hour expiration. Ideally this would be cached for 24 hours after the initial call.
    public String getTvdbToken() {
        Response response = given()
                .contentType("application/json")
                .body("{\n" +
                        "  \"apikey\": \"71F5C9B879536E85\",\n" +
                        "  \"userkey\": \"B57C75208FDB1453\",\n" +
                        "  \"username\": \"stan_test\"\n" +
                        "}")
                .when()
                .post("https://api.thetvdb.com/login");
        String token = response.path("token");
        String bearer = "Bearer ";
        return bearer.concat(token);
    }





}
