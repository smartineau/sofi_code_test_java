package tests;

/*
 * In picking a test I took an approach I've been using at work for writing tests. With little knowledge of the product
 * I looked for the first thing that would be high enough priority to start working on. For me, moving on something that's
 * viable and helps me get domain knowledge of the project has helped me find bigger issues in the past.
 *  For this test I focused on the external dependency, the tvdb. The other external services aren't
 * around anymore. The Movie DB is storing id's needed to get resources from tvdb, so I wrote tests to validate the references we are
 * storing and to make sure that if it disappears like the other ones it would be caught in a test.
 *
 *
 * While these tests are across multiple endpoints on the Movie DB, they are using the same external API for validations.
 */
import io.restassured.RestAssured;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class TheMovieDBTest extends TestBase {
    @BeforeClass
    public void setBaseUri () {
        RestAssured.baseURI = "https://api.themoviedb.org/3";
    }


    //The Movie DB is storing ID's used by external sources. This is a dependency not controlled by them
    // writing a check to assert that we can get the id's, and that they are correct on external site.

    /*
    *  Tests: Movies -> Does not have an external ID method like TV show, seasons, and episodes, to give you the id,
    *  but there is an endpoint exposed to search by external ID's. This would be worth testing.
    *
    *  Check for actors.
    *  1. Query MovieDB for the actors on a tv show and tvdb_id.
    *  2. Save the json object.
    *  3. Query tvdb and check.
    *
    *  tvdb also holds references to IMDB ids for some queries. IMDB does not have an api, using tvdb to check for could be a reasonable
    *  option.
    *
    *  Check series search with IMDB id.
    *   1. Query MovieDB for a series.
    *   2. Save the IMDB id and name.
    *   3. Query tvdb.
    *   4. Check name to verify key is same.
    *
    *   Check series episodes query with IMDB id
    *   1. Query MovieDB for series and episode
    *   2. Save IMDB id and result.
    *   3. Query tvdb.
    *   4. Check information for match
    */


    @Test
    public void testFindExternalIDsForITCrowdAndCallsThem() {
        Integer tvdb_id = given().
                when().
                get("/tv/2490/external_ids?language=en-US&api_key=" + api_key).
                then().
                body("id", equalTo(2490)).
                extract().path("tvdb_id");

        given().header("Authorization", getTvdbToken()).
                pathParam("tvdb_id", tvdb_id).
                when().get("https://api.thetvdb.com/series/{tvdb_id}").
                then().
                assertThat().statusCode(200).
                assertThat().body("data.seriesName", equalTo("The IT Crowd"));
    }
    @Test
    public void testFindExternalSeasonIDsForITCrowdAndCallsThem() {

        Integer tvdb_id = given()
                .when()
                .get("/tv/2490/season/1/episode/1/external_ids?api_key=" + api_key + "&language=en-US")
                .then()
                .assertThat().statusCode(200)
                .body("id", equalTo(189512))
                .extract().path("tvdb_id");

        given()
                .header("Authorization", getTvdbToken())
                .pathParam("tvdb_id", tvdb_id)
                .when().get("https://api.thetvdb.com/episodes/{tvdb_id}")
                .then()
                .assertThat().statusCode(200)
                .assertThat().body("data.episodeName", equalTo("Yesterday's Jam"));
    }

}
